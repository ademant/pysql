#!/bin/bash
mypath='dirname $(realpath $0)'
set -m
/usr/bin/mosquitto_sub -h banana -t "#" -v|while read line
do
	${mypath}/send_mqtt.sh ${line} &
done
