<!DOCTYPE html>
<html>
<head>
<title>Übersicht Wetter</title>
</head>
<body>
<p>Zeit: {{zeitstempel}}</p>
<table>
<tr><th>Ort</th> <th>Temperatur</th> <th>Luftfeuchte </th> <th>Luftdruck </th><th>lux</th><th>UV </th></tr>
<tr><th>CarPort</th> <th>{{tempdata['temp_carport']}}</th> <th>{{tempdata['hum_carport']}} </th> <th>{{tempdata['press_carport']}} </th><th></th></tr>
<tr><th>Wohnzimmer</th> <th>{{tempdata['temp_wohnen']}}</th> <th>{{tempdata['hum_wohnen']}} </th> <th>{{tempdata['press_wohnen']}} </th><th></th></tr>
<tr><th>Dach</th> <th>{{tempdata['temp_dach']}}</th> <th>{{tempdata['hum_dach']}} </th> <th>{{tempdata['press_dach']}} </th><th>{{tempdata['lux_dach']}}</th><th>{{tempdata['uv_dach']}}</th></tr>
<tr><th>Garten</th> <th>{{tempdata['temp_garten']}}</th> <th>{{tempdata['hum_garten']}} </th> <th>{{tempdata['press_garten']}} </th><th></th><th></th></tr>
<tr><th>Solarthermiemodul</th> <th>{{tempdata['temp_solar']}}</th> <th></th> <th></th><th></th><th></th></tr>
</table>
</body>
</html>
