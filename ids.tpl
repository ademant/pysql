<!DOCTYPE html>
<html>
<head>
<title>Messkanäle</title>
</head>
<body>
<p>Aktuelle Variablen:
<table>
<tr>
	<th>Interne ID</th> <th>Gerät</th> <th>Messgröße </th> <th>Sensor </th> <th>Adresse</th> <th> Anzahl Messwerte</th> <th> letzter Wert</th>
</tr>
%for item in measdata:
<tr>
	<td><a href="/graph/{{item['id']}}/hourly">{{item['id']}}</a></td> <td>{{item['device']}} </td> <td>{{item['varname']}} </td> <td>{{item['sensor']}} </td> <td>{{item['i2c']}}</td> <td> Anzahl: {{item['count']}}</td> <td> letzter Wert: {{item['value']}}</td>
</tr>
%end
</table>
</body>
</html>
