import time,zlib,gnupg,os,sys,pymysql,numpy,socket,json

#gpg=gnupg.GPG('/usr/bin/gpg',gnupghome='/home/pi/.gnupg')
gpg=gnupg.GPG()

pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)
configfile=abspath+"/config.json"
try:
	cf=open(configfile,"r")
except:
	cf=open(configfile+".template","r")

log_conf=json.load(cf)
cf.close()

parameter={"device":socket.gethostname()}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]

try:
	mydb=pymysql.connect(read_default_file="~/.my.cnf",database="rasolar")
except:
	print("no connection to database")
else:
	mycursor=mydb.cursor()
	mycursor.execute('select id, max(time) as time from measures group by id')
#	mycursor.execute("select distinct id from ids;")
	ti=mycursor.fetchall()
	for ids,lt in ti:
		myids=str(ids)
		data_dir="data/"+myids
		file_timestamp=data_dir+"/timestamp"
		file_measures=data_dir+"/measures"
		try:
			os.makedirs(data_dir,exist_ok=True)
		except:
			print("could not create data storage")
		else:
			brun=False
			try:
				stored_timestamps=int(os.path.getsize(file_timestamp)/8)
			except:
				select_string="select measures.time,measures.value from measures where id='"+myids+"' order by measures.time"
				brun=True
			else:
				f=open(file_timestamp,"rb")
				f.seek((stored_timestamps-1)*8)
				lasttime=int.from_bytes(f.read(8),byteorder="big")
				print(myids+" last timestamp: "+str(lasttime)+'/'+str(lt))
				if lasttime < int(lt):
					print("new values since "+str(lt))
					select_string="select measures.time,measures.value from measures where id='"+myids+"' and measures.time > "+str(lasttime)+" order by measures.time"
					brun=True
			if brun:
				no_rows=mycursor.execute(select_string)
				if(no_rows>0):
					tm=mycursor.fetchall()
					ts=open("data/"+myids+"/timestamp","ab")
					tv=open("data/"+myids+"/measures","ab")
					for tims,meas in tm:
						ts.write(int(tims).to_bytes(8,byteorder="big",signed=False))
						tv.write(int(meas).to_bytes(4,byteorder="big",signed=False))
					ts.close()
					tv.close()
	
