<!DOCTYPE html>
<html>
<head>
<title>Übersicht Tristar</title>
</head>
<body>
<p>Zeit: {{zeitstempel}}</p>
<p>Batteriespannung: {{solardata['volt_bat_sens']}}/{{solardata['volt_bat']}} V</p>
<p>Modulspannung: {{solardata['volt_sweep_mp']}}/{{solardata['volt_sweep_oc']}} V</p>
<p>Ladestrom: {{solardata['amp_bat']}} A</p>
<p>Temperatur: {{solardata['temp_heatsink']}} / {{solardata['temp_bat']}}</p>
<p>Leistung: {{solardata['kwh_tot']}} kwh_total</p>
</body>
</html>
