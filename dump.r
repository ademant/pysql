#!/usr/bin/r
library(RMySQL)
library(RSQLite)

con<-dbConnect(RMySQL::MySQL(),dbname="rasolar")
con2<-dbConnect(RSQLite::SQLite(),dbname="dump.sqlite")

for(i in dbListTables(con)){
	indata<-dbReadTable(con,i)
	dbWriteTable(con2,i,indata)
}
dbClose(con)
dbClose(con2)
